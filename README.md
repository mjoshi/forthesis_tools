# *Alag* and *templeRun*

This repository hosts the two tools developed for performing analysis of genetic variants using a combination of comparative and population genomics approaches. Here, *Alag*, is specifically used for performing analysis of the genetic variants occurring within the coding regions. *Alag* is mainly used for analysing the genetic variants occurring within the coding regions. Here, user can also perform this analysis on specific functional domains of interest occurring within the coding regions. *templeRun* is mainly a wrapper around *TEMPLE* (Litovchenko and Laurent, 2016) that streamlines the analysis of the noncoding regions through TEMPLE. 

##### Both Alag and templeRun are written in R, however, these tools also internally employ other external tools for performing various tasks.
